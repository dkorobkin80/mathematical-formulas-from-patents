import base64
import requests
import json
import re
import pyodbc
from meza import io
import adodbapi
from bs4 import BeautifulSoup, Tag
import pytesseract
import os
import subprocess
import psycopg2
import latex2mathml.converter
from bs4 import BeautifulSoup
import random
from fake_useragent import UserAgent
import requests
import time
import json
import re
import urllib3
from lxml import html
import socks
import socket
from stem import Signal
from stem.control import Controller
from torrequest import TorRequest
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout, QWidget, QTableWidget, QTableWidgetItem, QLCDNumber, QSlider, QVBoxLayout, QPushButton, QLineEdit
from PyQt5.QtCore import QSize, Qt
import re
import sqlite3
import datetime

def recognition(src):
    with open('newfile.jpg', 'wb') as target:
        a = requests.get(src)
        target.write(a.content)
    image_uri = "data:image/jpg;base64," + base64.b64encode(open('newfile.jpg', "rb").read()).decode()
    r = requests.post("https://api.mathpix.com/v3/latex",
             data=json.dumps({'src': image_uri}),
            headers={"app_id": "tolo_dima_gmail_com", "app_key": "3ff7b69ff55af5e5ffaa",
                    "Content-type": "application/json"})
    try:
        if  json.loads(r.text)['latex'] == "":
            return "распознать не удалось"
        else:
            return json.loads(r.text)['latex']
    except:
        return "распознать не удалось"

if __name__=="__main__":
    conn = psycopg2.connect(dbname='FE', user='postgres',
                            password='Nhblwfnmnhtnbq33', host='localhost')
    cursor = conn.cursor()

    cursor.execute('SELECT "URL" FROM public."Pat" WHERE "IDPat"='+sys.argv[1]+';')

    start_url = ""
    print(sys.argv)
    for a in cursor.fetchall():
        start_url = a[0]
    Equ = []
    print(start_url)
    Descr = []
    i = 1
    conn.commit()
    r1 = requests.get(start_url)
    cursor.execute('SELECT MAX("IDEqu") FROM public."Equ"')
    for a in cursor.fetchall():
        if a[0]:
         i = a[0] + 1
    soup1 = BeautifulSoup(r1.text, "html.parser")
   # print(soup1.find("div", "f3rblock", "clearfix"))
    for name in soup1.find_all("p"):
        k = 1
        if soup1.find_all("p").index(name) == (len(soup1.find_all("p")) - 1):
            break
        elif re.findall(r"<img.*?>", str(soup1.find_all()[soup1.find_all().index(name)])) and (re.findall(r"где", str(soup1.find_all("p")[soup1.find_all("p").index(name) + 1])) or re.findall(r"здесь", str(soup1.find_all("p")[soup1.find_all("p").index(name) + 1])) or re.findall(r"Здесь", str(soup1.find_all("p")[soup1.find_all("p").index(name) + 1])) or re.findall(r"Из", str(soup1.find_all("p")[soup1.find_all("p").index(name) + 1]))):
            equ = {}
            equ['equPic'] = re.sub("src=", "", re.sub("\"", "", re.findall(r"src=\".*?\"", re.findall(r"<img.*?>", str(soup1.find_all()[soup1.find_all().index(name)]))[0])[0]))
            equ['IDEqu'] = i
            t = soup1.find_all("p")[soup1.find_all("p").index(name) - k].text.split(".")
            equ['descEqu'] = t[-1]
            equ['equ'] = recognition(equ['equPic'])
            Equ.append(equ)

            text = str(soup1.find_all("p")[soup1.find_all("p").index(name) + 1])
            j = 1
            while j is not None:
                if  soup1.find_all("p")[soup1.find_all("p").index(name) + j].text.find(".") == -1 and not soup1.find_all("p").index(name) + j == (len(soup1.find_all("p")) - 1):
                    j += 1
                    text += str(soup1.find_all("p")[soup1.find_all("p").index(name) + j])

                else:
                    j = None
            for fragment in text.split(";"):
                if re.findall(r"<img.*?>", fragment):
                    for r in re.findall(r"<img.*?>", fragment):
                        desc = {}
                        desc["IDEqu"] = i
                        desc["descPic"] = re.sub("src=", "", re.sub("\"", "", re.findall(r"src=\".*?\"", r)[0]))

                        desc["descL"] = recognition(desc["descPic"])
                        re.sub("<img.*?src=\">" + desc["descPic"] + "\".*?/>", "КАРТИНКА", r)
                        soupFrag = BeautifulSoup(fragment, "html.parser")
                        desc['descT'] = soupFrag.text
                        Descr.append(desc)
                else:
                    desc = {}
                    desc["IDEqu"] = i
                    desc["descPic"] = 'нет картинки'
                    desc["descL"] = "нет формулы"
                    desc['descT'] = re.sub("<.*?/{0,1}>", "", fragment)
                    Descr.append(desc)
            z = 1
            while True:
                if re.findall(r"<img.*?>", str(soup1.find_all("p")[soup1.find_all("p").index(name) - z])):
                    equ = {}
                    i += 1
                    equ['equPic'] = re.sub("src=", "", re.sub("\"", "", re.findall(r"src=\".*?\"", re.findall(r"<img.*?>", str(soup1.find_all("p")[soup1.find_all("p").index(name) - z]))[0])[0]))
                    equ['IDEqu'] = i
                    t = soup1.find_all("p")[soup1.find_all("p").index(name) - k].text.split(".")
                    equ['descEqu'] = t[-1]
                    equ['equ'] = recognition(equ['equPic'])
                    if re.findall(">=<", recognition(equ['equPic'])) or re.findall("&asymp", recognition(equ['equPic'])):
                        Equ.append(equ)

                        text = str(soup1.find_all("p")[soup1.find_all("p").index(name) + 1])
                        j = 1
                        while j is not None:
                            if soup1.find_all("p")[soup1.find_all("p").index(name) + j].text.find(".") == -1:
                                j += 1
                                text += str(soup1.find_all("p")[soup1.find_all("p").index(name) + j])

                            else:
                                j = None
                        for fragment in text.split(";"):
                            if re.findall(r"<img.*?>", fragment):
                                for r in re.findall(r"<img.*?>", fragment):
                                    desc = {}
                                    desc["IDEqu"] = i
                                    desc["descPic"] = re.sub("src=", "",
                                                             re.sub("\"", "", re.findall(r"src=\".*?\"", r)[0]))

                                    desc["descL"] = recognition(desc["descPic"])
                                    re.sub("<img.*?src=\">" + desc["descPic"] + "\".*?/>", "КАРТИНКА", r)
                                    soupFrag = BeautifulSoup(fragment, "html.parser")
                                    desc['descT'] = soupFrag.text
                                    Descr.append(desc)
                            else:
                                desc = {}
                                desc["IDEqu"] = i
                                desc["descPic"] = 'нет картинки'
                                desc["descL"] = "нет формулы"
                                desc['descT'] = re.sub("<.*?/{0,1}>", "", fragment)
                                Descr.append(desc)
                    else:
                        i -= 1
                        break
                    z += 1
                else:
                    break
            i += 1

    with open("public/equ.json", "w") as write_file:
        write_file.write(json.dumps(Equ))
    with open("public/descr.json", "w") as write_file:
        write_file.write(json.dumps(Descr))
    conn.commit()
    conn.close()
    """   Equt = []
    for elem in Equ:
        print(re.sub("\'", "", elem['equ']))
        re.sub("'", "''", elem['equ'])
        s = ((elem['IDEqu'], (re.sub("\'", "",re.sub('"', '""', elem['equ']))), elem['equPic'], elem['descEqu']))
        cursor.execute('INSERT INTO public."Equ"("IDEqu", equ, "equPic", "descEqu") VALUES ' + str(s) + ";")



    Descrt = []
    for elem in Descr:
        Descrt.append((elem['IDEqu'], elem['descL'], elem['descT'], elem['descPic']))
    for elem in Descrt:
        cursor.execute('INSERT INTO public."Descr"("IDEqu", "descL", "descT", "descPic") VALUES ' + str(elem) + ";")"""


